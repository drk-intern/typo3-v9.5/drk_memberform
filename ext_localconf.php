<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_memberform',
    'Memberform',
    [
        \DRK\DrkMemberform\Controller\MemberFormController::class => 'showMemberForm, send',
    ],
    // non-cacheable actions
    [
        \DRK\DrkMemberform\Controller\MemberFormController::class => 'send',
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_memberform',
    'Donationform',
    [
        \DRK\DrkMemberform\Controller\MemberFormController::class => 'showDonationForm, send',
    ],
    // non-cacheable actions
    [
        \DRK\DrkMemberform\Controller\MemberFormController::class => 'send',
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);
