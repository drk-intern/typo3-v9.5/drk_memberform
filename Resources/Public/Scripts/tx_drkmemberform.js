/**
 *
 * DRK Memberform Javascript
 *
 */

$(function () {
  'use strict';

  let termsCheck = $('#terms_check');
  let btnSubmit = $('#sendform');
  if (termsCheck.length && !termsCheck.is(':checked')) {
    btnSubmit.prop('disabled', true);
  }

  termsCheck.click(function () {
    if (termsCheck.is(':checked')) {
      btnSubmit.prop('disabled', false);
    } else {
      btnSubmit.prop('disabled', true);
    }
  });

  let formtextFields = $('#memberform input.o-form__field--text');
  formtextFields.change(function () {
    toggleTextFieldLabel(this);
  });


  $('.donation-form__amount-button').click(function () {
      $('.donation-amount').val($(this).attr('value'));
      return false;
    }

  );

  setTimeout(
    function () {

      //trigger toggleFirmaCourseRegistration on load
      tx_memberform_toggleFirma();
      useSEPA();
    }, 100);

  let iban = $('#memberform #iban');
  if (iban.length > 0) {
    setTimeout(
      function () {
        iban
          .mask('aa99 9999 9999 9999 9999 99', {
            placeholder: 'DE__ ____ ____ ____ ____ __'
          })
          .focusout(function () {
            $(this).attr('placeholder', '');
          });
      }, 100);
  }

});

/**
 * useKontonr
 */
function useKontonr() {
  $('#sepa_kontonr input').prop('disabled', true);
  $('#klassische_kontonr input').prop('disabled', false);
  $('#klassische_kontonr').show();
  $('#sepa_kontonr').hide();
}

/**
 * useSEPA
 */
function useSEPA() {
  $('#sepa_kontonr input').prop('disabled', false);
  $('#klassische_kontonr input').prop('disabled', true);
  $('#klassische_kontonr').hide();
  $('#sepa_kontonr').show();
}

function tx_memberform_toggleFirma() {
  if ($('#memberform').hasClass('multiform')) {
    if ($('input#person').is(':checked')) {
      $('.tx-drk-memberform-company').hide();
      $('.tx-drk-memberform-person').show();
      $('.tx-drk-memberform-person input, .tx-drk-memberform-person select').prop('disabled', false);
      $('.tx-drk-memberform-company input, .tx-drk-memberform-company select').prop('disabled', true);
    } else {
      $('.tx-drk-memberform-person').hide();
      $('.tx-drk-memberform-company').show();
      $('.tx-drk-memberform-person input, .tx-drk-memberform-person select').prop('disabled', true);
      $('.tx-drk-memberform-company input, .tx-drk-memberform-company select').prop('disabled', false);
    }
  }
}

function toggleTextFieldLabel(field) {
  if (field) {
    if (field.value) $(field).addClass('has-value');
    else $(field).removeClass('has-value');
  }
}
