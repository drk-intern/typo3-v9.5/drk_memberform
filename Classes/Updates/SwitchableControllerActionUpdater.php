<?php
namespace DRK\DrkMemberform\Updates;

use DRK\DrkGeneral\Updates\AbstractSwitchableControllerActionsPluginUpdater;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;

#[UpgradeWizard('drkmemberform_memberform-SwitchableControlleractionUpdater')]
class SwitchableControllerActionUpdater extends AbstractSwitchableControllerActionsPluginUpdater
{
    protected const MIGRATION_SETTINGS = [
        [
            'sourceListType' => 'drkmemberform_memberform',
            'switchableControllerActions' => 'MemberForm->showMemberForm;MemberForm->send',
            'targetListType' => '',
            'targetCtype' => 'drkmemberform_memberform'
        ], [
            'sourceListType' => 'drkmemberform_memberform',
            'switchableControllerActions' => 'MemberForm->showDonationForm;MemberForm->send',
            'targetListType' => '',
            'targetCtype' => 'drkmemberform_donationform'
        ],
    ];
}
