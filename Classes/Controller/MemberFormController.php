<?php

namespace DRK\DrkMemberform\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Controller\AbstractDrkController;
use DRK\DrkGeneral\Utilities\Utility;
use DRK\DrkMemberform\Domain\Repository\MemberFormRepository;
use TYPO3\CMS\Core\Page\AssetCollector;
use Psr\Http\Message\ResponseInterface;

/**
 * MemberFormController
 */
class MemberFormController extends AbstractDrkController
{

    /**
     * @var MemberFormRepository $memberFormRepository
     */
    protected MemberFormRepository $memberFormRepository;

    /**
     * Donation period
     *
     * @var array
     * should match with kdb option
     */
    protected array $donationperiodeArray = [
        5 => "Einmalig",
        4 => "Jährlich",
        3 => "Halbjährlich",
        2 => "Vierteljährlich",
        1 => "Monatlich"
    ];

    /**
     * Donation Purpose
     *
     * @var array
     */
    protected array $donationpurposeArray = [];

    /**
     * Contribution period for membership
     *
     * @var array
     * should match with kdb option
     */
    protected array $contributionperiodeArray = [
        4 => "Jährlich",
        3 => "Halbjährlich",
        2 => "Vierteljährlich",
        1 => "Monatlich",
    ];

    protected $mandatoryFormFields = [
        'person' => ['vorname', 'name', 'strasse', 'plz', 'ort'],
        'company' => ['vorname', 'name', 'firma', 'strasse', 'plz', 'ort']
    ];

    /**
     * @param AssetCollector $assetCollector
     */
    public function __construct(
        private readonly AssetCollector $assetCollector
    ) {
    }

    /**
     * @param MemberFormRepository $memberFormRepository
     */
    public function injectMemberFormRepository(MemberFormRepository $memberFormRepository): void
    {
        $this->memberFormRepository = $memberFormRepository;
    }

    /**
     * action showMemberForm
     *
     * @return ResponseInterface
     */
    public function showMemberFormAction(): ResponseInterface
    {
        $this->showForm();
        return $this->htmlResponse();
    }

    /**
     * @param bool $isDonation
     */
    protected function showForm(bool $isDonation = false): void
    {
        $orgData = $this->memberFormRepository->getOrgData([$this->settings['apiKey']]);

        if ($orgData) {
            $this->view->assign('Error', false);
        } else {
            $this->view->assign('Error', true);
            $this->error_reporting();
        }

        $this->view->assign('orgData', $orgData);
        $this->view->assign('prefix_array', Utility::$prefixArray + [4 => 'Familie']);
        $this->view->assign('title_array', Utility::$titleArray);
        $this->view->assign('ShowBirthdayField', $this->settings['showBirthdayField']);
        $this->view->assign('ShowPhoneField', $this->settings['showPhoneField']);
        $this->view->assign('bPayingCash', $this->settings['payCash']);
        $this->view->assign('showEmailDeclassificationField', $this->settings['showEmailDeclassificationField']);
        $this->view->assign(
            'terms_url',
            $this->validateUrl($this->settings['termsUrl']) ? $this->settings['termsUrl'] : ''
        );
        $this->view->assign(
            'privacy_url',
            $this->validateUrl($this->settings['privacyUrl']) ? $this->settings['privacyUrl'] : ''
        );

        $dues = explode(',', $this->settings['dues']);
        foreach ($dues as &$d) {
            trim($d);
        }
        $this->view->assign('dues', $dues);
        $this->view->assign('isCompany', (bool)$this->settings['companyVersion']);

        if ($isDonation) {
            if (!empty($this->settings['donatePurpose'])) {
                $this->donationpurposeArray = explode(',', $this->settings['donatePurpose']);
            } else {
                $this->donationpurposeArray = ['Rotkreuzarbeit'];
            }
            $this->view->assign('donationpurposeArray', $this->donationpurposeArray);
            $this->view->assign('donationperiodeArray', $this->donationperiodeArray);
        } else {
            $this->view->assign('contributionperiodeArray', $this->contributionperiodeArray);
        }
    }

    /**
     * error_reporting
     */
    private function error_reporting(): void
    {
        $errors = array_merge($this->error, $this->memberFormRepository->getErrors());
        $this->view->assign('errorMessages', $errors);
    }

    /**
     * action showDonationForm
     *
     * @return ResponseInterface
     */
    public function showDonationFormAction(): ResponseInterface
    {
        $this->showForm(true);
        return $this->htmlResponse();
    }

    /**
     * action send
     *
     * @return ResponseInterface
     */
    public function sendAction(): ResponseInterface
    {
        $aFormData = [];
        $this->view->assign('sending_ok', false);
        $this->view->assign('debug', $this->settings['debug']);
        $aArguments = $this->request->getArguments();
        $isCompany = 0;

        $errors = false;

        if (!empty($aArguments)) {
            array_key_exists(
                'anrede',
                $aArguments
            ) ? $aFormData['anrede'] = $aArguments['anrede'] : $aFormData['anrede'] = 1;
            array_key_exists(
                'titel',
                $aArguments
            ) ? $aFormData['titel'] = $aArguments['titel'] : $aFormData['titel'] = 0;
            array_key_exists(
                'iscompany',
                $aArguments
            ) ? $aFormData['spendertyp'] = $aArguments['iscompany'] : $aFormData['spendertyp'] = 0;
            array_key_exists('name', $aArguments) ? $aFormData['name'] = $aArguments['name'] : $aFormData['name'] = "";
            array_key_exists(
                'vorname',
                $aArguments
            ) ? $aFormData['vorname'] = $aArguments['vorname'] : $aFormData['vorname'] = "";
            array_key_exists(
                'firma',
                $aArguments
            ) ? $aFormData['firma'] = $aArguments['firma'] : $aFormData['firma'] = "";
            array_key_exists(
                'strasse',
                $aArguments
            ) ? $aFormData['strasse'] = $aArguments['strasse'] : $aFormData['strasse'] = "";
            array_key_exists('ort', $aArguments) ? $aFormData['ort'] = $aArguments['ort'] : $aFormData['ort'] = "";
            array_key_exists('plz', $aArguments) ? $aFormData['plz'] = $aArguments['plz'] : $aFormData['plz'] = "";
            array_key_exists(
                'email',
                $aArguments
            ) ? $aFormData['email'] = $aArguments['email'] : $aFormData['email'] = "";
            array_key_exists(
                'donation_amount',
                $aArguments
            ) ? $aFormData['donation_amount'] = $aArguments['donation_amount'] : $aFormData['donation_amount'] = 0;
            array_key_exists(
                'donation_periode',
                $aArguments
            ) ? $aFormData['donation_periode'] = $aArguments['donation_periode'] : $aFormData['donation_periode'] = "5";
            array_key_exists(
                'bank_accountowner',
                $aArguments
            ) ? $aFormData['bank_accountowner'] = $aArguments['bank_accountowner'] : $aFormData['bank_accountowner'] = "";
            array_key_exists(
                'kontotyp',
                $aArguments
            ) ? $aFormData['kontotyp'] = $aArguments['kontotyp'] : $aFormData['kontotyp'] = "SEPA";
            array_key_exists(
                'kontonummer',
                $aArguments
            ) ? $aFormData['kontonummer'] = $aArguments['kontonummer'] : $aFormData['kontonummer'] = "";
            array_key_exists('blz', $aArguments) ? $aFormData['blz'] = $aArguments['blz'] : $aFormData['blz'] = "";
            array_key_exists('iban', $aArguments) ? $aFormData['iban'] = $aArguments['iban'] : $aFormData['iban'] = "";
            array_key_exists('bank', $aArguments) ? $aFormData['bank'] = $aArguments['bank'] : $aFormData['bank'] = "";
            array_key_exists(
                'email_freigabe',
                $aArguments
            ) ? $aFormData['email_freigabe'] = $aArguments['email_freigabe'] : $aFormData['email_freigabe'] = 0;
            array_key_exists(
                'kundenbeziehung',
                $aArguments
            ) ? $aFormData['kundenbeziehung'] = $aArguments['kundenbeziehung'] : $aFormData['kundenbeziehung'] = 46;
            array_key_exists(
                'geburtsdatum',
                $aArguments
            ) ? $aFormData['geburtsdatum'] = $aArguments['geburtsdatum'] : $aFormData['geburtsdatum'] = null;
            array_key_exists(
                'telefon',
                $aArguments
            ) ? $aFormData['telefon'] = $aArguments['telefon'] : $aFormData['telefon'] = "";

            // Set donation purpose if donation form has send
            if ($aFormData['kundenbeziehung'] == 48) {
                if (!empty($this->settings['donatePurpose'])) {
                    $this->donationpurposeArray = explode(',', $this->settings['donatePurpose']);
                } else {
                    $this->donationpurposeArray = ['Rotkreuzarbeit'];
                }

                if (array_key_exists($aArguments['donation_purpose'], $this->donationpurposeArray)) {
                    $aFormData['spendenzweck'] = $this->donationpurposeArray[$aArguments['donation_purpose']];
                } else {
                    $aFormData['spendenzweck'] = 'Rotkreuzarbeit';
                }
            } else {
                $aFormData['spendenzweck'] = '';
            }

            if (array_key_exists('iscompany', $aArguments) && intval($aArguments['iscompany']) == 1) {
                $isCompany = 1;
            }
        }

        //check term-check
        if (
            $this->validateUrl($this->settings['termsUrl']) &&
            (!array_key_exists('terms_check', $aArguments) || intval($aArguments['terms_check']) != 1)
        ) {
            $this->error = ['Error' => 'Sie haben den AGB nicht zugestimmt.'];
            $errors = true;
        }

        //check if we have a empty form
        $sCheck = $aArguments['name'] . $aArguments['vorname'] . $aArguments['firma'] ??= '' . $aArguments['bank_accountowner'];
        if (empty($sCheck)) {
            $this->error = array('Error' => 'Bitte kein leeres Formular absenden!');
            $errors = true;
        }

        if (!$isCompany) {
            $mandatoryFieldsFilled = $this->checkMandatoryFormFields($this->mandatoryFormFields['person'], $aFormData);
        } else {
            $mandatoryFieldsFilled = $this->checkMandatoryFormFields($this->mandatoryFormFields['company'], $aFormData);
        }

        //check honeypot field
        if (!$this->isHoneypotFilled($aArguments['birthname']))
        {
            $errors = true;
        }

        if (!$errors && $mandatoryFieldsFilled) {
            $aOrgData = $this->memberFormRepository->getOrgData([$this->settings['apiKey']]);

            $request = [
                $this->settings['apiKey'],
                $aFormData
            ];

            // sending Data are successful
            if ($this->sendCustomerData($request)) {
                $this->view->assign('sending_ok', true);

                // if successPageId is set, the redirect to this page
                if ($this->settings['successPageId'] && !$this->settings['debug']) {
                    $uriBuilder = $this->uriBuilder;
                    $uriBuilder->reset();
                    $uriBuilder->setTargetPageUid($this->settings['successPageId']);
                    $uri = $uriBuilder->build();
                    $this->redirectToUri($uri);
                    exit;
                } // else go on and render the template
                else {
                    unset($request);

                    $aClientData = $aFormData;
                    $aClientData['anrede'] = array_key_exists(
                        $aFormData['anrede'],
                        Utility::$prefixArray
                    ) ? Utility::$prefixArray[$aFormData['anrede']] : "Herr";
                    $aClientData['titel'] = array_key_exists(
                        $aFormData['titel'],
                        Utility::$titleArray
                    ) ? Utility::$titleArray[$aFormData['titel']] : "";

                    $aClientData['donation_periode'] = $this->donationperiodeArray[$aFormData['donation_periode']];
                    $aClientData['donation_amount_per_year'] = $aClientData['donation_amount'];

                    $this->view->assign('aClientData', $aClientData);
                    $this->view->assign('aOrgData', $aOrgData);
                    $this->view->assign('isCompany', $isCompany);
                }
            }
        }

        // if Error, then show it now
        if (!empty($this->error)) {
            $this->view->assign('error', true);
            $this->error_reporting();
        } else {
            $this->view->assign('error', false);
        }

        if ($this->settings['debug']) {
            $this->view->assign('debug', $this->request->getArguments());
        }
        return $this->htmlResponse();
    }

    /**
     *
     * sendCustomerData
     *
     * @param array $aSendObject
     * @return bool
     * @throws \Exception
     */
    private function sendCustomerData($aSendObject = []): bool
    {
        if (empty($aSendObject)) {
            $this->error = ['Error' => 'No object to send!'];
            return false;
        }

        $client = $this->memberFormRepository->getJsonClient();
        $response = $client->execute('sendCustomerData', $aSendObject);

        //catch Service Error
        if (!empty($response)) {
            if ($response['status'] != "OK") {
                // translate error messages
                switch ($response['message']) {
                    case "IBAN is not valid":
                        $this->error = ['Error' => "Leider wurde ein Fehler in Ihrer IBAN oder der Kontoverbindung festgestellt!"];
                        break;
                    default:
                        $this->error = ['Error' => "Der Webservice meldet: " . $response['message']];
                }
                return false;
            } else {
                return true;
            }
        } else {
            $this->error = ['Error' => 'Webservice antwortet nicht oder fehlerhaft!'];
            return false;
        }
    }

    /**
     * Init
     *
     * @return void
     */
    protected function initializeAction(): void
    {
        parent::initializeAction();

        $this->assetCollector->addStyleSheet(
            'memberform',
            'EXT:drk_memberform/Resources/Public/Css/styles.css'
        );

        $this->assetCollector->addJavaScript(
            'memberform',
            'EXT:drk_memberform/Resources/Public/Scripts/tx_drkmemberform.js',
            [],
            ['priority' => false] // lädt in footer

        );

    }

}
