<?php

namespace DRK\DrkMemberform\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Domain\Repository\AbstractDrkRepository;
use Exception;

/**
 * The repository for MemberForms
 */
class MemberFormRepository extends AbstractDrkRepository
{

    /**
     *
     * getOrgData
     *
     * @param array $request
     *
     * @return bool
     */
    public function getOrgData($request = [])
    {

        $cacheIdentifier = sha1(json_encode(['drk_memberform|getOrgData', $request]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        try {
            $orgData = $this->executeJsonClientAction('getOrgData', $request);

            if (isset($orgData['org_name'])) {

                /**
                 * manipulate org name
                 */

                // clear DRK or BRK from Name
                if (strpos(
                    strtolower($orgData['org_name']),
                    'drk'
                ) !== false || strpos(strtolower($orgData['org_name']), 'brk') !== false) {
                    $orgData['org_name'] = str_ireplace('DRK', '', $orgData['org_name']);
                    $orgData['org_name'] = str_ireplace('BRK', '', $orgData['org_name']);
                }

                // now set the right prefix
                if ((bool)$this->settings['isBRK']) {
                    $orgData['org_name'] = "BRK " . $orgData['org_name'];
                } else {
                    $orgData['org_name'] = "DRK " . $orgData['org_name'];
                }
            }

            $this->getCache()->set($cacheIdentifier, $orgData, [], $this->heavy_cache);

            return $orgData;
        } catch (Exception $exception) {
            $this->error = ['Fehler' => $exception->getMessage()];
            return false;
        }
    }
}
