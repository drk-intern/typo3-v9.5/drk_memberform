<?php

// Memberform Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_memberform',
    'Memberform',
    'LLL:EXT:drk_memberform/Resources/Private/Language/locallang.xlf:tt_content.memberform_plugin.title',
    'EXT:drk_memberform/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Mitgliedschaft und Spenden'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers'
    ]
);

// Donationform Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_memberform',
    'Donationform',
    'LLL:EXT:drk_memberform/Resources/Private/Language/locallang.xlf:tt_content.donationform_plugin.title',
    'EXT:drk_memberform/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Mitgliedschaft und Spenden'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers'
    ]
);
